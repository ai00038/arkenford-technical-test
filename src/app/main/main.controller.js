(function() {
	'use strict';
	
	angular
		.module('main')
			.controller('MainController', MainController);
			
	function MainController($http){
		var vm = this;

		vm.loading = true;
		vm.rgbToHex = rgbToHex;
		vm.renderColourWithTintInHex = renderColourWithTintInHex;
		vm.chooseColour = chooseColour;
		vm.addTintsAndCap = addTintsAndCap;
		vm.createColour = createColour;
		vm.saveCurrentPalette = saveCurrentPalette;
		vm.importPaletteFromJSON = importPaletteFromJSON;

		vm.selectedColour = {
			name: '',
			baseColour: [0, 0, 0],
			tints: generateDefaultTints()
		}; // default colour black

		activate();

		function activate(){
			$http({
		 	 method: 'GET',
		 	 url: 'https://qbs.arkonline.co.uk/task/colours.json?task=2'
			}).then(successCallback, errorCallback);
		}

		function importPaletteFromJSON(paletteInJSON){
			vm.colours = JSON.parse(paletteInJSON);
		}

		function saveCurrentPalette(){
			return angular.toJson(vm.colours);
		}

		function createColour(newColour){
			vm.colours.push(angular.copy(newColour));
		}

		function chooseColour(colourArr){
			vm.selectedColour.baseColour = angular.copy(colourArr);
		}

		function successCallback(response) {
			vm.colours = response.data;
			vm.loading = false;
		}

		function errorCallback(response) {
			console.log(response);
		}

		function componentToHex(c) {
		    var hex = c.toString(16);
		    return hex.length == 1 ? "0" + hex : hex;
		}

		function rgbToHex(r, g, b) {
		    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
		}
		

		function addTintsAndCap(baseColorRGB, tintRGB){
			var RGB_COLOUR_DIMENSIONS = 3,
				retArr = [];

			for(var i = 0; i < RGB_COLOUR_DIMENSIONS; i++){
				retArr.push(capRGBValue(baseColorRGB[i]+tintRGB[i]));
			}

			return retArr;
		}

		function capRGBValue(val){
			if(val < 0){
				val = 0;
			} else if(val > 255){
				val = 255;
			}

			return val;
		}

		function renderColourWithTintInHex(colourArr, tintArr){
			var tintedAndCappedRGB = addTintsAndCap(colourArr, tintArr);
			return rgbToHex(tintedAndCappedRGB[0], tintedAndCappedRGB[1], tintedAndCappedRGB[2]);
		}

		function generateDefaultTints(){
			return [[-23,-24,-28],[-11,-11,-7],[39,31,18],[83,70,39],[116,99,54]];
		}

	}
	
})();